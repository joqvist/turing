# JastAdd Turing Machine

A Turing machine can be modeled with Higher-Order attributes in a Reference Attribute Grammar.
This is an implementation using JastAdd.

Run the tests with `./gradlew check`

Run the demo with `./gradlew build; java -jar turing.jar`
