package org.jastadd.turing;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Tape object for a Turing machine.
 */
public class Tape extends java.lang.Object {

  public final java.util.List<Character> tape;
  public final int offset;

  public Tape() {
    this.tape = new ArrayList<>();
    this.offset = 0;
  }

  public Tape(Collection<Character> tape, int offset) {
    this.tape = new ArrayList<>(tape);
    this.offset = offset;
  }

  public Tape(Tape tape) {
    this(tape.tape, tape.offset);
  }

  public Tape(String tape) {
    this.tape = new ArrayList<>(tape.length());
    this.offset = 0;
    for (int i = 0; i < tape.length(); ++i) {
      this.tape.add(tape.charAt(i));
    }
  }

  @Override public String toString() {
    StringBuilder buf = new StringBuilder(tape.size());
    for (Character ch : tape) {
      buf.append(ch);
    }
    return buf.toString();
  }

  public Character scan(int head, char blank) {
    int pos = offset + head;
    if (pos >= 0 && pos < tape.size()) {
      return tape.get(pos);
    } else {
      return blank;
    }
  }

  /**
   * Print a symbol to the tape at the given head position.
   * If this expands the tape, blank symbols are added in the expanded places.
   */
  public Tape print(int head, char symbol, char blank) {
    int pos = offset + head;
    if (pos < 0) {
      // Move offset.
      ArrayList<Character> newTape = new ArrayList<>(-pos + tape.size());
      newTape.add(symbol);
      for (int i = 1; i < -pos; ++i) {
        newTape.add(blank);
      }
      newTape.addAll(tape);
      return new Tape(newTape, offset - pos);
    } else {
      ArrayList<Character> newTape = new ArrayList<>(tape);
      while (pos >= newTape.size()) {
        newTape.add(blank);
      }
      newTape.set(pos, symbol);
      return new Tape(newTape, offset);
    }
  }


}
