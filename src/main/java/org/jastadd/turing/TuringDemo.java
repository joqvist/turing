package org.jastadd.turing;

public class TuringDemo {
  public static void main(String[] args) {
    System.out.println("Welcome to the Turing Machine demo.");

    /*System.out.println("Running countUp...");
    run(countUp());

    System.out.println("Running countDown...");
    run(countDown());

    System.out.println("Running busy beaver...");
    run(busyBeaver());*/

    //System.out.println("Running plus one...");
    //run(plusOne("10011."));

    System.out.println("Adding 3 and 10...");
    run(adder("11+1010."));

    System.out.println("Adding 11 and 7...");
    run(adder("1011+111."));
  }

  /**
   * Prints 123 with three states.
   */
  public static TuringMachine countUp() {
    TuringMachine machine = new TuringMachine();
    machine.setBlank('0');
    machine.setConfiguration(new Configuration(new Tape(), 0, 0));
    machine.addInstruction(new Instruction(0, '0', new Print('1'), new Right(), 1));
    machine.addInstruction(new Instruction(1, '0', new Print('2'), new Right(), 2));
    machine.addInstruction(new Instruction(2, '0', new Print('3'), new Stay(), Instruction.HALT));
    return machine;
  }

  /**
   * Prints 321 with three states.
   */
  public static TuringMachine countDown() {
    TuringMachine machine = new TuringMachine();
    machine.setBlank('0');
    machine.setConfiguration(new Configuration(new Tape(), 0, 0));
    machine.addInstruction(new Instruction(0, '0', new Print('1'), new Left(), 1));
    machine.addInstruction(new Instruction(1, '0', new Print('2'), new Left(), 2));
    machine.addInstruction(new Instruction(2, '0', new Print('3'), new Left(), Instruction.HALT));
    return machine;
  }

  public static TuringMachine busyBeaver2() {
    TuringMachine machine = new TuringMachine();
    machine.setBlank('0');
    machine.setConfiguration(new Configuration(new Tape(), 0, 0));
    machine.addInstruction(new Instruction(0, '0', new Print('1'), new Right(), 1));
    machine.addInstruction(new Instruction(0, '1', new Print('1'), new Left(), 2));
    machine.addInstruction(new Instruction(1, '0', new Print('1'), new Left(), 0));
    machine.addInstruction(new Instruction(1, '1', new Print('1'), new Right(), 1));
    machine.addInstruction(new Instruction(2, '0', new Print('1'), new Left(), 1));
    machine.addInstruction(new Instruction(2, '1', new Print('1'), new Right(), Instruction.HALT));
    return machine;
  }

  private static void run(TuringMachine machine) {
    Configuration conf = machine.getConfiguration();
    System.out.println(conf.toString());
    while (!conf.isHalted()) {
      conf = conf.next();
      System.out.println(conf.toString());
    }
  }

  /**
   * A turing machine that adds one to a binary number.
   * The number ends with a decimal point.
   */
  public static TuringMachine plusOne(String tape) {
    TuringMachine machine = new TuringMachine();
    machine.setBlank('0');
    machine.setConfiguration(new Configuration(new Tape(tape), 0, 0));

    // State 0: find the end of the number.
    machine.addInstruction(new Instruction(0, '0', new None(), new Right(), 0));
    machine.addInstruction(new Instruction(0, '1', new None(), new Right(), 0));
    machine.addInstruction(new Instruction(0, '.', new None(), new Left(), 1));

    // State 1: add one.
    machine.addInstruction(new Instruction(1, '0', new Print('1'), new Stay(), Instruction.HALT));
    machine.addInstruction(new Instruction(1, '1', new Print('0'), new Left(), 1));
    return machine;
  }

  /**
   * A turing machine that adds two binary numbers.
   * The numbers are separated by '+' and the last one ends with a decimal point.
   */
  public static TuringMachine adder(String tape) {
    TuringMachine machine = new TuringMachine();
    machine.setBlank('0');
    machine.setConfiguration(new Configuration(new Tape(tape), 0, 0));

    // State 0: find last digit of first number.
    machine.addInstruction(new Instruction(0, '0', new None(), new Right(), 0));
    machine.addInstruction(new Instruction(0, '1', new None(), new Right(), 0));
    machine.addInstruction(new Instruction(0, '+', new None(), new Left(), 1));

    // State 1: mark the last digit of the first number as the working digit.
    machine.addInstruction(new Instruction(1, '0', new Print('o'), new Right(), 2));
    machine.addInstruction(new Instruction(1, '1', new Print('s'), new Right(), 2));

    // State 2: find the end of the second number.
    machine.addInstruction(new Instruction(2, '0', new None(), new Right(), 2));
    machine.addInstruction(new Instruction(2, '1', new None(), new Right(), 2));
    machine.addInstruction(new Instruction(2, 'o', new None(), new Right(), 2));
    machine.addInstruction(new Instruction(2, 's', new None(), new Right(), 2));
    machine.addInstruction(new Instruction(2, '+', new None(), new Right(), 2));
    machine.addInstruction(new Instruction(2, '.', new None(), new Left(), 3));

    // State 3: remove the last digit of the second number.
    machine.addInstruction(new Instruction(3, '0', new Print('.'), new Left(), 4));
    machine.addInstruction(new Instruction(3, '1', new Print('.'), new Left(), 6));

    // No more digits in the second number. Reset marked digit in first number.
    machine.addInstruction(new Instruction(3, '+', new Print('.'), new Left(), 9));

    // State 4: advance marked digit in first number.
    machine.addInstruction(new Instruction(4, '0', new None(), new Left(), 4));
    machine.addInstruction(new Instruction(4, '1', new None(), new Left(), 4));
    machine.addInstruction(new Instruction(4, '+', new None(), new Left(), 4));
    machine.addInstruction(new Instruction(4, 'o', new Print('0'), new Left(), 5));
    machine.addInstruction(new Instruction(4, 's', new Print('1'), new Left(), 5));

    machine.addInstruction(new Instruction(5, '0', new Print('o'), new Right(), 2));
    machine.addInstruction(new Instruction(5, '1', new Print('s'), new Right(), 2));

    // State 6: add one to marked digit in first number.
    machine.addInstruction(new Instruction(6, '0', new None(), new Left(), 6));
    machine.addInstruction(new Instruction(6, '1', new None(), new Left(), 6));
    machine.addInstruction(new Instruction(6, '+', new None(), new Left(), 6));

    // Easy case: just replace o with 1.
    machine.addInstruction(new Instruction(6, 'o', new Print('1'), new Left(), 5));

    // Tricky case: replace s with 0, then increment and mark next digit, then do carry.
    machine.addInstruction(new Instruction(6, 's', new Print('0'), new Left(), 7));

    // State 7: increment and mark, then carry if needed.
    machine.addInstruction(new Instruction(7, '0', new Print('s'), new Right(), 2));
    machine.addInstruction(new Instruction(7, '1', new Print('o'), new Left(), 8));

    // State 8: carry cycle.
    machine.addInstruction(new Instruction(8, '0', new Print('1'), new Right(), 2));
    machine.addInstruction(new Instruction(8, '1', new Print('0'), new Left(), 8));

    // State 9: reset marked digit in first number then halt.
    machine.addInstruction(new Instruction(9, '0', new None(), new Left(), 9));
    machine.addInstruction(new Instruction(9, '1', new None(), new Left(), 9));
    machine.addInstruction(new Instruction(9, '+', new None(), new Left(), 9));
    machine.addInstruction(new Instruction(9, 'o', new Print('0'), new Stay(), Instruction.HALT));
    machine.addInstruction(new Instruction(9, 's', new Print('1'), new Stay(), Instruction.HALT));
    return machine;
  }
}
