package org.jastadd.turing;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestTuringMachine {

  /** Test a machine that writes 123. */
  @Test public void testSimple1() {
    TuringMachine machine = TuringDemo.countUp();
    Configuration stop = machine.run(100);
    assertEquals("123", stop.getTape().toString());
  }

  /** Test a machine that writes 321. */
  @Test public void testSimple1Rev() {
    TuringMachine machine = TuringDemo.countDown();
    Configuration stop = machine.run(100);
    assertEquals("321", stop.getTape().toString());
  }

  /**
   * Test a two-state busy beaver.
   */
  @Test public void testBB2() {
    TuringMachine machine = TuringDemo.busyBeaver2();
    Configuration stop = machine.run(100);
    assertEquals("111111", stop.getTape().toString());
    assertEquals(1, stop.getHead());

    // Run too few steps.
    Configuration early = machine.run(5);
    assertEquals("1111", early.getTape().toString());
  }

  @Test public void testPlus1() {
    assertEquals("10100.", run(TuringDemo.plusOne("10011."), 100));
    assertEquals("10000000.", run(TuringDemo.plusOne("1111111."), 100));
    assertEquals("1001.", run(TuringDemo.plusOne("1000."), 100));
  }

  @Test public void testAdd() {
    assertStartsWith("10.", run(TuringDemo.adder("1+1."), 100));
    assertStartsWith("011.", run(TuringDemo.adder("1+10."), 100));
    assertStartsWith("11.", run(TuringDemo.adder("10+1."), 100));
    assertStartsWith("100.", run(TuringDemo.adder("11+1."), 100));
    assertStartsWith("100.", run(TuringDemo.adder("1+11."), 100));

    // 3 + 12 = 15
    assertStartsWith("01101.", run(TuringDemo.adder("11+1010."), 100));

    // 17 + 8 = 25
    assertStartsWith("11001.", run(TuringDemo.adder("10001+1000."), 100));

    // 11 + 7 = 18
    assertStartsWith("10010.", run(TuringDemo.adder("1011+111."), 100));
  }

  private void assertStartsWith(String expected, String actual) {
    if (!actual.startsWith(expected)) {
      fail(String.format("%s does not start with %s", actual, expected));
    }
  }

  private String run(TuringMachine machine, int steps) {
    Configuration stop = machine.run(steps);
    return stop.getTape().toString();
  }

}
